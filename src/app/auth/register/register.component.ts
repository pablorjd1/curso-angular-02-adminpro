import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2'


import { UsuarioService } from '../../services/usuario.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public formSubmitted = false;

  public registerForm = this.fb.group({
    nombre:['Pablo',[Validators.required,Validators.minLength(3)]],
    email:['prjd@gmail.com',[Validators.required,Validators.email]],
    password:['123456',[Validators.required]],
    password2:['123456',[Validators.required]],
    terminos:[true,[Validators.required]],

  }, {
    validators: this.passwordsIguales('password','password2')
  } as AbstractControlOptions);

  constructor(private fb: FormBuilder,
    private usuarioService:UsuarioService
    ) { }



  ngOnInit(): void {
  }

  crearUsuario(){
    this.formSubmitted = true;
    console.log(this.registerForm.value)
    console.log(this.registerForm.valid)
    if (!this.registerForm.valid) {
      return
    }
    this.usuarioService.crearUsuario( this.registerForm.value )
        .subscribe(resp => {
          console.log(resp);
        }, (err)=>{
          console.warn(err)
          Swal.fire('Error',err.error.msg,'error')
        })
  }

  campoNoValido(campo:string):boolean {

    if (this.registerForm.get(campo).invalid  && this.formSubmitted ) {
      return true;
    }else {
      return false;
    }
  }

  contrasenasNoValidas(){
    const pass1 = this.registerForm.get("password").value;
    const pass2 = this.registerForm.get("password2").value;

    if ((pass1 !== pass2) && this.formSubmitted ) {
      return true ;
    }else {
      return false;
    }


  }
  passwordsIguales(pass1Name:string,pass2Name:string) {

    return (formGroup: FormGroup) => {

      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      console.log(pass1Control,"pass1Control")
      console.log(pass2Control,"pass2Control")

      if (pass1Control.value === pass2Control.value) {
          pass2Control.setErrors(null)
      } else {
          pass2Control.setErrors({ noEsIgual: true })
      }

    }

  }

}

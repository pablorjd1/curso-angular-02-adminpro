import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//import externos
import { ChartsModule } from "ng2-charts";
//modulos personalizados
import { PagesModule } from "./pages/pages.module";
import { AuthModule } from "./auth/auth.module";
import { SharedModule } from "./shared/shared.module";
import { AppRoutingModule } from "./app-routing.module";


import { AppComponent } from './app.component';
import { NotpagefoundComponent } from './notpagefound/notpagefound.component';




@NgModule({
  declarations: [
    AppComponent,
    NotpagefoundComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    AuthModule,
    SharedModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

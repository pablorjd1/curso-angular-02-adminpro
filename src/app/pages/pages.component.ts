import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../services/settings.service';


//llamada a funciones desde fuera de la aplicacion de angular
declare function  customInitFuncion();

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {
  constructor( private settingsService:SettingsService ) { }
  ngOnInit(): void {
    customInitFuncion();


  }

}

import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html',
  styleUrls: []
})
export class PromiseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const promesas = new Promise((resolve,reject)=>{
      if (false) {
        resolve('Hola muin')
      }else {
        reject('algo salio mal')
      }
    })

    promesas.then((mens)=>{
      console.log('%cpromise.component.ts line:20 Promesa resulta', 'color: #007acc;',mens)
    }).catch((err)=>{
      console.log('%cpromise.component.ts line:26 algo salio mal ', 'color: #007acc;', err );
    })

    this.getUsuarios().then( user => {
      console.log('%cpromise.component.ts line:31 user', 'color: #007acc;', user);
    })
  }
  getUsuarios(){
    const prome = new Promise(resolve => {
      fetch('https://reqres.in/api/users')
      .then(resp => resp.json())
      .then(body => resolve(body.data))
    })
    return prome;

  }

}

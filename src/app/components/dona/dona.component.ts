import { Component, Input, OnInit } from '@angular/core';

import { ChartType } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent implements OnInit {

  @Input('labels') doughnutChartLabels: Label[] = ['test1','test1','test1'];

  public doughnutChartData: MultiDataSet = [];
  public colors: Color[] =[
    { backgroundColor: ['#9E120E','#FF5800','#FFB414'] }
  ]

  // public doughnutChartType: ChartType = 'doughnut';

  @Input() title:string = 'Sin Titulo'

  @Input() data

  constructor() { }

  ngOnInit(): void {
  }

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
